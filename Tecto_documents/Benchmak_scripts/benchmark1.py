# Md Sabber Ahamed and Eunseo Choi
# Center for Erathquake Research and Information(CERI)
# The University of Memphis
# Benchmark-1 of Energy Balance equation. Details of theory given in the paper
# In this Benchmark we included only plastic power term in the energy
# balance equation.
######################################################################################

import numpy
from numpy import pi, sin, sqrt, exp
import matplotlib.pyplot as plt
# matplotlib inline

sys.path.append('..')
from Dynearthsol import Dynearthsol


# Vector initializing
total_length = 2001
edotxx       = numpy.zeros(total_length)
stress_xx    = numpy.zeros(total_length)
stress_zz    = numpy.zeros(total_length)
pressure_ana = numpy.zeros(total_length)
temp_ana     = numpy.zeros(total_length)
rho          = numpy.zeros(total_length)


# Parameters
mu    = 200e6 # Shear modulus
K     = 200e6 # Bulk Modulus
vx    = -1e-5 #Velocity applied
c_p   = 1000 # Specific heat
rho0  = 1.0 # Density
alpha = 3e-5 # expansion coefficient
phi   = 10*pi/180
psi   = 10*pi/180
coh   = 1e6 # Cohesion

e1    = K+4.0*mu/3.0 # lambda+2*mu
e2    = K-2.0*mu/3.0 # lambda
sf    = sin(phi)
nf    = (1.0+sf)/(1.0-sf)
sp    = sin(psi)
np    = (1.0+sp)/(1.0-sp)

L = 1.0
vxL = (abs(vx)/L)
A = vx/L*(e1 - e2*nf)/( (e1 + e2) * nf * np + 2.0 * e1 - 2.0 * e2 * (nf + np) )
tcr = 2.0*coh*L*sqrt(nf)/((e1-e2*nf)*abs(vx))
X = e1*(vx/L-2.0*A)+2.0*e2*np*A
Y = (2.0*A*e1-2.0*A*e2*np)*tcr
W = e1*np*A+e2*(vx/L-2.0*A+np*A)
Z = (-e1*np*A+e2*(2.0*A-np*A))*tcr


time = numpy.linspace(tcr, tcr+1.5e-2/abs(vx), total_length)
for i in range(total_length):
    k=A/(rho0*c_p)
    temp_ana[i] = k*((X-np*W)*(time[i]**2-tcr**2) + 2.0*(Y-np*Z)*(time[i]-tcr))
    stress_xx[i] = X*time[i]+Y



displp = time*abs(vx)*1e2
disple = numpy.linspace(0,displp[0],11)
fig, (ax1) = plt.subplots(1,1,figsize=(5,5))
plt.rcParams.update({'font.size': 12})

ax1.plot(displp,temp_ana+273.0,'k-',linewidth=2)
ax1.plot(disple,numpy.zeros(len(disple))+273.0,'k-',linewidth=2)
ax1.set_xlim([0,2.0])
ax1.set_ylim([270.0,288.0])
ax1.set_title('Temperature change due to plastic power',size=14)
ax1.set_xlabel('Displacement (x$10^{-2}$ m)',size=14)
ax1.set_ylabel('Temperature (K)',size=14)
plt.show()

#ax2.plot(displp,numpy.abs(stress_xx)*1.0e-6,'k-',linewidth=2)
#ax2.set_xlim([0,2.0])
##ax2.set_ylim([270.0,288.0])
#ax2.set_title('Temperature change due to plastic power',size=14)
#ax2.set_xlabel('Displacement (x$10^{-2}$ m)',size=14)
#ax2.set_ylabel('$\sigma_{xx}$ (MPa)',size=16)
#ax2.plot(displ,numpy.abs(stress_xx)*1.0e-6)
