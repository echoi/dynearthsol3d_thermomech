# Md Sabber Ahamed and Eunseo Choi
# Center for Erathquake Research and Information(CERI)
# The University of Memphis
# Benchmark-2 of Energy Balance equation. Details of theory given in the paper
# In this Benchmark we included all the terms in the energy
# balance equation to test the full temperature evolution
######################################################################################

import sys
import numpy as np
import matplotlib.pyplot as plt
from math import pi, sin, sqrt, exp

sys.path.append('..')
from Dynearthsol import Dynearthsol

# Parameters
mu    = 200e6 # Shear modulus
K     = 200e6 # Bulk Modulus
L     = 1.0   # Edge length of the cube.
vx    = -1e-5 #Velocity applied
c_p   = 1000 # Specific heat
rho0  = 1.0 # Density
alpha = 3e-5 # expansion coefficient
phi   = 10*pi/180
psi   = 10*pi/180
coh   = 1e6 # Cohesion

e1    = K+4.0*mu/3.0
e2    = K-2.0*mu/3.0
sf    = sin(phi)
nf    = (1.0+sf)/(1.0-sf)
sp    = sin(psi)
np1   = (1.0+sp)/(1.0-sp)
dT = 0

# Vector initializing

total_length = 2001
edotxx       = np.zeros(total_length)
stress_xx    = np.zeros(total_length)
stress_zz    = np.zeros(total_length)
pressure_ana = np.zeros(total_length)
temp_ana     = np.zeros(total_length) + 273.16
rho          = np.ones(total_length) * rho0


# Calculation:
displacement = abs(vx) * np.array(range(total_length), dtype=float)
for i in range(1, total_length):
    #ts          = (alpha * dT)/3.0 #thermal strain
    de          = ((vx) / (1 - displacement[i]));
    edot        = (vx) / (1 - displacement[i])
    #yield_time  = (2.0 * coh * sqrt(nf)) /((e1 - e2 * nf) * abs(vx))
    yield_time  = L/vx*(exp(-(2.0*coh*sqrt(nf))/((1.0-nf)*e2+2.0*mu))-1.0)
    beta        = ((e1 - e2 * nf) * de) / ((1 * (e1 + e2) * nf * np1) - (2.0 * e2 * (nf + np1)) + (2.0 * e1))

    if  (i < yield_time):
        #Elastic stress:
        stress_xx[i]  = stress_xx[i-1] + (e1*de)
        stress_zz[i]  = stress_zz[i-1] + (e2*de)
        plastic_power = 0
    else:
        #Plastic stress:
        stress_xx[i]  = stress_xx[i-1] + ((de * e1) - (2 * e1 * beta) + (2 * e2 * np1 * beta))
        stress_zz[i]  = stress_zz[i-1] + ((e1 * np1 * beta) + (de * e2) + (e2 * beta * (np1 - 2)))

        #Plastic strain:
        ep_xx = (2 * beta)
        ep_yy = (-beta * np1)
        ep_zz = (-beta * np1)
        plastic_power = (stress_xx[i] * ep_xx) + (stress_zz[i] * ep_zz) + (stress_zz[i] * ep_zz)

#Pressure:
    pressure_ana[i] = -(stress_xx[i]+stress_zz[i]+stress_zz[i])/3.0
    dp = pressure_ana[i] - pressure_ana[i-1]

#Density:
    rho[i] = rho0*(L/(L-displacement[i]))
    #rho[i] = rho[i-1]*(1 - edot)

#Temperature:
    tmass               = (c_p * rho[i-1]) + (alpha * pressure_ana[i-1])
    plastic_power_term  = plastic_power/tmass
    density_term        = (temp_ana[i-1] * pressure_ana[i-1] * alpha * edot) / tmass
    pressure_term       = (alpha * dp * temp_ana[i-1]) / tmass
    temp_ana[i]         = temp_ana[i-1] + plastic_power_term + pressure_term + density_term
    #dT                  = temp_ana[i] - temp_ana[i-1]


# Numerical data from Dynearthsol3D :


def numerical():
    des    = Dynearthsol('DES3D_solutions/result')
    dis_n  = np.zeros(51, dtype=float)
    Sxx_n  = np.zeros(51, dtype=float)
    Szz_n  = np.zeros(51, dtype=float)
    temp_n = np.zeros(51, dtype=float)
    rho_n  = np.zeros(51, dtype=float)

    fo = open("B2_DES3D.dat","w")
    for i in range(51):
        des.read_header(i)
        s = des.read_field(i, 'stress')
        T = des.read_field(i, 'temperature')
        r = des.read_field(i, 'density')
        x = des.read_field(i, 'coordinate')
        dis_n[i] = 1 - x[3,0]

        #print abs(s[1,1]) and temp
        Sxx_n[i]  = abs(s[1,0])
        Szz_n[i]  = abs(s[1,1])
        temp_n[i] = abs(T[1])
        rho_n[i]  = abs(r[1])
        print >> fo, "%e %e %e %e %e" % (dis_n[i], Sxx_n[i], Szz_n[i], temp_n[i], rho_n[i])
    fo.close()
    return dis_n, Sxx_n, Szz_n, temp_n, rho_n

displacement_nn, Sxx_nn, Szz_nn, temp_n, rho_n = numerical()

##Ploting results :
#fignum=1
#plt.close(fignum)
#plt.figure(fignum)
#plt.subplot(1,2,1)
#plt.plot(displacement*1e3, temp_ana,'k-',displacement_nn[::3]*1e3, temp_n[::3],'ko')
#plt.xlabel('Displacement (meter $10^{-3}$)')
#plt.ylabel('Temperature (K)')
# plt.legend(['Analytical', 'DES3D'], loc='lower right')

#plt.subplot(1,2,2)
#plt.plot(displacement*1e3, rho,'k-', displacement_nn[::3]*1e3, rho_n[::3], 'ko')
#plt.xlabel('Displacement (meter $10^{-3}$)')
#plt.ylabel('Density($kg/m^3$)')
#plt.legend(['Analytical', 'DES3D'], loc='lower right')

#plt.show()
