# Sabber Ahamed and Eunseo Choi
# Center for Erathquake Research and Information(CERI)
# The University of Memphis
# Benchmark-3 of Energy Balance equation. Details of theory given in the paper
# In this Benchmark we included Thermal stress with all the terms in the energy
# balance equation to test temperature<>rheology mutual feedback
######################################################################################

import sys
import numpy as np
import matplotlib.pyplot as plt
from math import pi, sin, sqrt

sys.path.append('..')
from Dynearthsol import Dynearthsol


# Vector initializing

total_length = 2001
stress_xx    = np.zeros(total_length)
stress_zz    = np.zeros(total_length)
pressure_ana = np.zeros(total_length)
temp_ana     = np.zeros(total_length)


# Parameters
mu    = 200e6 # Shear modulus
K     = 200e6 # Bulk Modulus
vx    = -1e-5 #Velocity applied
c_p   = 1000 # Specific heat
alpha = 3e-5 # Thermal expansion coefficient
phi   = 10*pi/180
psi   = 10*pi/180
coh   = 1e6 # Cohesion

e1    = K+4.0*mu/3.0
e2    = K-2.0*mu/3.0
sf    = sin(phi)
nf    = (1.0+sf)/(1.0-sf)
sp    = sin(psi)
np1   = (1.0+sp)/(1.0-sp)

c_p      = 1000 # Specific heat capacity
alpha    = 3.0e-5
temp_ana[0] = 273
domain_length = 1
dT = 0

#Calculation
displacement = abs(vx) * np.array(range(total_length), dtype=float)
for i in range(1, total_length):
    # temperature
    lin_term    = (2 * 2e-2 *1)
    temp_ana[i] = temp_ana[i-1] + lin_term
    dT          = temp_ana[i] - temp_ana[i-1]   # Temperature difference between two time step
    delta_T     = temp_ana[i] - 273             #Temperature difference between current and initial time step

    l           = (domain_length-displacement[i])
    de          = (vx) / (1 - displacement[i]);
    yield_time  = (2.0 * coh * sqrt(nf) + (K * alpha*delta_T * (nf-1)) * 1)/((nf*e2-e1)*vx)
    beta        = (((e1 - e2 * nf) * de) + (K * alpha * dT * (nf - 1))) / (((e1 + e2) * nf * np1) - (2.0 * e2 * (nf + np1)) + (2.0 * e1))

    if  (i < yield_time):
       stress_xx[i] = stress_xx[i-1] + (e1 * de) - (K * alpha * dT)
       stress_zz[i] = stress_zz[i-1] + (e2 * de) - (K * alpha * dT)
    else:
       stress_xx[i] = stress_xx[i-1] + ((de * e1) - (2 * e1 * beta) + (2 * e2 * np1 * beta)) - (K * alpha * dT)
       stress_zz[i] = stress_zz[i-1] + ((e1 * np1 * beta) +(de * e2) + (e2 * beta * (np1 - 2))) - (K * alpha * dT)



# Numerical data from Dynearthsol3D :
def numerical():
    des    = Dynearthsol('DES3D_solutions/result')
    dis_n  = np.zeros(51, dtype=float)
    Sxx_n  = np.zeros(51, dtype=float)
    Szz_n  = np.zeros(51, dtype=float)
    temp_n = np.zeros(51, dtype=float)
    rho_n  = np.zeros(51, dtype=float)

    fo = open("B3_DES3D.dat","w")
    for i in range(51):
        des.read_header(i)
        s = des.read_field(i, 'stress')
        T = des.read_field(i, 'temperature')
        r = des.read_field(i, 'density')
        x = des.read_field(i, 'coordinate')
        dis_n[i] = 1 - x[3,0]

        #print abs(s[1,1]) and temp
        Sxx_n[i]  = abs(s[1,0])
        Szz_n[i]  = abs(s[1,1])
        temp_n[i] = abs(T[1])
        rho_n[i]  = abs(r[1])
        print >> fo, "%e %e %e %e" % (dis_n[i], Sxx_n[i], Szz_n[i], rho_n[i])
    fo.close()
    return dis_n, Sxx_n, Szz_n, temp_n, rho_n


displacement_nn, Sxx_nn, Szz_nn, temp_n, rho_n = numerical()


#Ploting results :
fignum=1
plt.close(fignum)
plt.figure(fignum)
plt.plot(displacement*1e3, temp_ana,'k-',displacement_nn[::3]*1e3, temp_n[::3],'ko')
plt.xlabel('Displacement (meter $10^{-3}$)')
plt.ylabel('Temperature (K)')
plt.legend(['Analytical', 'DES3D'], loc='lower right')
plt.show()


fignum=2
plt.close(fignum)
plt.figure(fignum)
plt.subplot(1,2,1)
plt.plot(displacement*1e3, abs(stress_xx)/1e6,'k-',displacement_nn[::3]*1e3, abs(Sxx_nn[::3])/1e6,'ko' )
plt.xlabel('Displacement (meter $10^{-3}$)')
plt.ylabel('|$\sigma_{xx}$| (MPa)')
# plt.legend(['Analytical', 'DES3D'], loc='lower right')

plt.subplot(1,2,2)
plt.plot(displacement*1e3,abs(stress_zz)/1e6,'k-', displacement_nn[::3]*1e3, abs(Szz_nn[::3])/1e6,'ko' )
plt.xlabel('Displacement (meter $10^{-3}$)')
plt.ylabel('|$\sigma_{zz}$| (MPa)')
plt.legend(['Analytical', 'DES3D'], loc='lower right')

plt.show()
