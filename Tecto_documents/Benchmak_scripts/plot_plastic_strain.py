# plot
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pickle
import numpy as np


pickle_file_400 = pickle_file_name = 'plastic_strain_400_resolution.pickle'
pickle_file_800 = pickle_file_name = 'plastic_strain_800_resolution.pickle'


file_400 = open(pickle_file_400, 'rb')
file_800 = open(pickle_file_800, 'rb')

data_400 = pickle.load(file_400, encoding='latin1')
data_800 = pickle.load(file_800, encoding='latin1')

avg_pstarin_400 = data_400['avg_pstrain']
max_pstrain_400 = data_400['max_pstrain']

avg_pstarin_800 = data_800['avg_pstrain']
max_pstrain_800 = data_800['max_pstrain']

model_name = ['diffusion', 'energy', 'fixed', 'variable', 'deviatoric']
models = ['DZN', 'FZT', 'FFT', 'FRT', 'FRD']
num_of_models = len(models)
number_of_files = 400


fig = plt.figure(figsize=(10,8))
font_size = 16
plt.rcParams.update({'font.size': font_size})
extensions = np.linspace(0, 40, number_of_files)
# rcParams['axes.titlepad'] = 20
plt.rc('text', usetex=True)
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.35)

for i in range(num_of_models):
    
    # Average plastic strain 400
    plt.subplot(2,2,1)
    plt.plot(extensions, avg_pstarin_400[i, :]/1e2, label=str(models[i]), linewidth=3)
    plt.title(r'\textbf{(a)} Avg. plastic strain (400m)', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Plastic Strain', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)
    plt.legend(loc=2)

    # Average plastic strain 800
    plt.subplot(2,2,2)
    plt.plot(extensions, avg_pstarin_800[i, :]/1e2, linewidth=3)
    plt.title(r'\textbf{(b)} Avg. plastic strain (800m)', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Plastic Strain', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)
    # plt.legend(loc=2)

    # Maximum plastic strain 400
    plt.subplot(2,2,3)
    plt.plot(extensions, max_pstrain_400[i, :], linewidth=3)
    plt.title(r'\textbf{(c)} Max. plastic strain (400m)', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Plastic Strain', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

    # Maximum plastic strain 800
    plt.subplot(2,2,4)
    plt.plot(extensions, max_pstrain_800[i, :], linewidth=3)
    plt.title(r'\textbf{(d)} Max. plastic strain (800m)', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Plastic Strain', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

plt.tight_layout()
plt.show()
# plt.savefig('../energy_figures/plastic_strain_evolution.eps')