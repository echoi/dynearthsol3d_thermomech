# plot
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pickle
import numpy as np


pickle_file_400 = pickle_file_name = 'power_no_strain_weakening.pickle'


file = open(pickle_file_400, 'rb')
data = pickle.load(file, encoding='latin1')

max_dev = data['max_dev']
max_vol = data['max_vol']
max_total = data['max_total']

avg_dev = data['avg_dev']
avg_vol = data['avg_vol']
avg_total = data['avg_total']

model_name = ['energy', 'fixed', 'variable', 'deviatoric']
models = ['FZT', 'FFT', 'FRT', 'FRD']
num_of_models = len(models)
number_of_files = 400


fig = plt.figure(figsize=(10,6))
font_size = 16
plt.rcParams.update({'font.size': font_size})
extensions = np.linspace(0, 40, number_of_files)
# rcParams['axes.titlepad'] = 20
plt.rc('text', usetex=True)
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.35)

for i in range(num_of_models):
    
    # Average plastic strain 400
    plt.subplot(1,3,1)
    plt.plot(extensions, max_dev[i, :]/1e6, label=str(models[i]), linewidth=3)
    plt.title(r'\textbf{(a)} Deviatoric power', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Deviatoric plastic power (MJ)', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)
    
    plt.legend(loc=2)

    # Average plastic strain 800
    plt.subplot(1,3,2)
    plt.plot(extensions, max_vol[i, :]/1e6, linewidth=3)
    plt.title(r'\textbf{(b)} Volumetric power', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Volumetric plastic power (MJ)', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    plt.subplot(1,3,3)
    plt.plot(extensions, max_total[i, :]/1e6, linewidth=3)
    plt.title(r'\textbf{(b)} Total power', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'Total plastic power (MJ)', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

    # # Average plastic strain 400
    # plt.subplot(2,3,4)
    # plt.plot(extensions, avg_dev[i, :], linewidth=3)
    # plt.title(r'\textbf{(a)} Avg. dev. power', loc='left', y=1.05)
    # plt.xlabel(r'Extension (km)', size=font_size)
    # plt.ylabel(r'Deviatoric plastic power', size=font_size)
    # plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

    # # Maximum plastic strain 800
    # plt.subplot(2,3,5)
    # plt.plot(extensions, avg_vol[i, :], linewidth=3)
    # plt.title(r'\textbf{(b)} avg. vol. power', loc='left', y=1.05)
    # # plt.xlabel(r'Extension (km)', size=font_size)
    # plt.ylabel(r'Volumetric plastic power', size=font_size)
    # plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

    # plt.subplot(2,3,6)
    # plt.plot(extensions, avg_total[i, :], linewidth=3)
    # plt.title(r'\textbf{(b)} Avg. total. power', loc='left', y=1.05)
    # # plt.xlabel(r'Extension (km)', size=font_size)
    # plt.ylabel(r'Total plastic power', size=font_size)
    # plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

plt.tight_layout()
plt.show()
# plt.savefig('../energy_figures/plastic_strain_evolution.eps')