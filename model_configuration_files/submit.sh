#!/bin/sh
qsub submit_energy.sh
qsub submit_deviatoric.sh
qsub submit_fixed.sh
qsub submit_variable.sh
qsub submit_energy_800.sh
qsub submit_deviatoric_800.sh
qsub submit_fixed_800.sh
qsub submit_variable_800.sh
